[Global]
cluster-id = ${cluster_name}

[VirtualCenter "${vsphere_vcenter}"]
insecure-flag = "true"
user = "${vsphere_user}"
password = "${vsphere_password}"
port = "443"
datacenters = "${vsphere_datacenter}"