provider "nsxt" {
    host                 = var.nsx_host
    username             = var.vsphere_user
    password             = var.vsphere_password
    allow_unverified_ssl = true
}

data "nsxt_transport_zone" "overlay_transport_zone" {
  display_name = "nest-overlay-vds"
}

data "nsxt_edge_cluster" "EC" {
  display_name = var.nsxt_edge_cluster
}

data "nsxt_logical_tier0_router" "tier0_router" {
  display_name = var.nsxt_t0
}

resource "nsxt_logical_router_link_port_on_tier0" "t0_port" {
  display_name      = "${var.cluster_name}_t0"
  logical_router_id = data.nsxt_logical_tier0_router.tier0_router.id
}

resource "nsxt_logical_tier1_router" "rke" {
  display_name    = "${var.cluster_name}_t1"
  edge_cluster_id = data.nsxt_edge_cluster.EC.id
  enable_router_advertisement = true
  advertise_connected_routes  = false
  advertise_static_routes     = true
  advertise_nat_routes        = true
  advertise_lb_vip_routes     = true
  advertise_lb_snat_ip_routes = false
}

resource "nsxt_logical_router_link_port_on_tier1" "t1_port" {
  logical_router_id             = nsxt_logical_tier1_router.rke.id
  linked_logical_router_port_id = nsxt_logical_router_link_port_on_tier0.t0_port.id
}

### Kube API Load Balancer

resource "nsxt_lb_service" "lb_service" {
  description  = "kubernetes load balancer"
  display_name = var.cluster_name

  enabled           = true
  logical_router_id = nsxt_logical_tier1_router.rke.id
  error_log_level   = "INFO"
  size              = "SMALL"

  virtual_server_ids = [nsxt_lb_tcp_virtual_server.kube_api.id, nsxt_lb_tcp_virtual_server.ingress_https.id, nsxt_lb_tcp_virtual_server.ingress_http.id]
  depends_on = [nsxt_logical_router_link_port_on_tier1.t1_port]
}

# Kube API Master Group
resource "nsxt_ns_group" "kube_api" {
    description     = "Kube API for ${var.cluster_name}"
    display_name    = "Kube-API-${var.cluster_name}"

    membership_criteria {
        target_type = "VirtualMachine"
        scope       = "k8s/${var.cluster_name}"
        tag         = "master"
    }
}

resource "nsxt_ns_group" "ingress-https" {
    description     = "https ingress for ${var.cluster_name}"
    display_name    = "ingress-https-${var.cluster_name}"

    membership_criteria {
        target_type = "VirtualMachine"
        scope       = "k8s/${var.cluster_name}"
        tag         = "worker"
    }
}

resource "nsxt_ns_group" "ingress-http" {
    description     = "http ingress for ${var.cluster_name}"
    display_name    = "ingress-http-${var.cluster_name}"

    membership_criteria {
        target_type = "VirtualMachine"
        scope       = "k8s/${var.cluster_name}"
        tag         = "worker"
    }
}

resource "nsxt_lb_tcp_monitor" "kube_api_tcp_monitor" {
  description  = "Health Monitor for Kube API Servers"
  display_name = "kubeapi_6443_tcp_monitor-${var.cluster_name}"
  fall_count   = 3
  interval     = 5
  monitor_port = 6443
  rise_count   = 3
  timeout      = 10
}

resource "nsxt_lb_tcp_monitor" "ingress_https_tcp_monitor" {
  description  = "Health Monitor for ingress https Servers"
  display_name = "ingress_443_tcp_monitor-${var.cluster_name}"
  fall_count   = 3
  interval     = 5
  monitor_port = 443
  rise_count   = 3
  timeout      = 10
}

resource "nsxt_lb_tcp_monitor" "ingress_http_tcp_monitor" {
  description  = "Health Monitor for ingress http Servers"
  display_name = "ingress_80_tcp_monitor-${var.cluster_name}"
  fall_count   = 3
  interval     = 5
  monitor_port = 80
  rise_count   = 3
  timeout      = 10
}

resource "nsxt_lb_fast_tcp_application_profile" "lb_fast_tcp_profile" {
  description       = "lb_fast_tcp_application_profile provisioned by Terraform"
  display_name      = "${var.cluster_name}_lb_fast_tcp_application_profile"
  close_timeout     = "8"
  idle_timeout      = "1800"
  ha_flow_mirroring = "false"
}

# Kube API LB Pool
resource "nsxt_lb_pool" "kube_api_pool" {
    description     = "Pool for ${var.cluster_name} KUBEAPI"
    display_name    = "${var.cluster_name}-kubeapi"
    
    snat_translation {
        type = "SNAT_AUTO_MAP"
    }
    
    active_monitor_id = nsxt_lb_tcp_monitor.kube_api_tcp_monitor.id

    member_group {
        ip_version_filter = "IPV4"
        port              = "6443"

        grouping_object {
            target_type = "NSGroup"
            target_id   = nsxt_ns_group.kube_api.id
        }
    }
}

resource "nsxt_lb_pool" "ingress_https_pool" {
    description     = "Pool for ${var.cluster_name} HTTPS ingress"
    display_name    = "${var.cluster_name}-ingress-https"
    
    snat_translation {
        type = "SNAT_AUTO_MAP"
    }
    
    active_monitor_id = nsxt_lb_tcp_monitor.ingress_https_tcp_monitor.id

    member_group {
        ip_version_filter = "IPV4"
        port              = "443"

        grouping_object {
            target_type = "NSGroup"
            target_id   = nsxt_ns_group.ingress-https.id
        }
    }
}

resource "nsxt_lb_pool" "ingress_http_pool" {
    description     = "Pool for ${var.cluster_name} HTTP ingress"
    display_name    = "${var.cluster_name}-ingress-http"
    
    snat_translation {
        type = "SNAT_AUTO_MAP"
    }
    
    active_monitor_id = nsxt_lb_tcp_monitor.ingress_http_tcp_monitor.id

    member_group {
        ip_version_filter = "IPV4"
        port              = "80"

        grouping_object {
            target_type = "NSGroup"
            target_id   = nsxt_ns_group.ingress-http.id
        }
    }
}

# Kube API Virtual Server
resource "nsxt_lb_tcp_virtual_server" "kube_api" {
    description = "Virtual Server for ${var.cluster_name} Kube API"
    display_name = "${var.cluster_name}-kubeapi"
    enabled = true
    ip_address = var.kube_api_ip
    ports = ["6443"]
    pool_id = nsxt_lb_pool.kube_api_pool.id
    application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
}

resource "nsxt_lb_tcp_virtual_server" "ingress_https" {
    description = "Virtual Server for ${var.cluster_name} HTTPS Ingress"
    display_name = "${var.cluster_name}-ingress-https"
    enabled = true
    ip_address = var.kube_api_ip
    ports = ["443"]
    pool_id = nsxt_lb_pool.ingress_https_pool.id
    application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
}

resource "nsxt_lb_tcp_virtual_server" "ingress_http" {
    description = "Virtual Server for ${var.cluster_name} HTTP Ingress"
    display_name = "${var.cluster_name}-ingress-http"
    enabled = true
    ip_address = var.kube_api_ip
    ports = ["80"]
    pool_id = nsxt_lb_pool.ingress_http_pool.id
    application_profile_id = nsxt_lb_fast_tcp_application_profile.lb_fast_tcp_profile.id
}

resource "nsxt_vm_tags" "rke-master" {
    count = var.rancher_count_master
    instance_id = vsphere_virtual_machine.rke-master[count.index].id
    tag {
        scope = "k8s/${var.cluster_name}"
        tag = "master"
    }
}

resource "nsxt_vm_tags" "rke-worker" {
    count = var.rancher_count_worker
    instance_id = vsphere_virtual_machine.rke-worker[count.index].id
    tag {
        scope = "k8s/${var.cluster_name}"
        tag = "worker"
    }
}