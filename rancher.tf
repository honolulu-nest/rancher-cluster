provider "rancher2" {
  api_url = "https://${var.rancher_api_url}/v3"
  token_key = var.rancher_api_token
  insecure = true
}

data "template_file" "csi-vsphere" {
    template = file("templates/csi-vsphere.conf.tpl")
    vars = {
        vsphere_user = var.vsphere_user
        vsphere_password = var.vsphere_password
        vsphere_vcenter = var.vsphere_vcenter
        vsphere_datacenter = var.vsphere_datacenter
        cluster_name = var.cluster_name
    }
}

data "template_file" "rke_addons" {
    template = file("templates/rke-addons.tpl")
    vars = {
        vsphere_user = base64encode(var.vsphere_user)
        vsphere_password = base64encode(var.vsphere_password)
        vsphere_vcenter = var.vsphere_vcenter
        vsphere_datacenter = var.vsphere_datacenter
        csi-vsphere = base64encode(data.template_file.csi-vsphere.rendered)
    }
}

resource "rancher2_cluster" "cluster" {
  name = var.cluster_name

  rke_config {
    network {
      plugin = "canal"
    }
    services {
      kubelet {
          extra_args = {
              "cloud-provider" = "external"
          }
      }
    }
    addons = data.template_file.rke_addons.rendered
  }
  
  enable_cluster_monitoring = true

  cluster_monitoring_input {
    answers = {
      "exporter-kubelets.https" = true
      "exporter-node.enabled" = true
      "exporter-node.ports.metrics.port" = 9796
      "exporter-node.resources.limits.cpu" = "200m"
      "exporter-node.resources.limits.memory" = "200Mi"
      "grafana.persistence.enabled" = false
      "grafana.persistence.size" = "10Gi"
      "grafana.persistence.storageClass" = "default"
      "operator.resources.limits.memory" = "500Mi"
      "prometheus.persistence.enabled" = "false"
      "prometheus.persistence.size" = "50Gi"
      "prometheus.persistence.storageClass" = "default"
      "prometheus.persistent.useReleaseName" = "true"
      "prometheus.resources.core.limits.cpu" = "1000m",
      "prometheus.resources.core.limits.memory" = "1500Mi"
      "prometheus.resources.core.requests.cpu" = "750m"
      "prometheus.resources.core.requests.memory" = "750Mi"
      "prometheus.retention" = "12h"
    }
  }

}

resource "rancher2_cluster_sync" "cluster" {
  cluster_id =  rancher2_cluster.cluster.id
  wait_monitoring = rancher2_cluster.cluster.enable_cluster_monitoring
  depends_on = [vsphere_virtual_machine.rke-master,vsphere_virtual_machine.rke-worker]
}

resource "rancher2_namespace" "istio" {
  name = "istio-system"
  project_id = rancher2_cluster_sync.cluster.system_project_id
  description = "istio namespace"
}

resource "rancher2_app" "istio" {
  catalog_name = "system-library"
  name = "cluster-istio"
  project_id = rancher2_namespace.istio.project_id
  template_name = "rancher-istio"
  target_namespace = rancher2_namespace.istio.id
  answers = {
    "certmanager.enabled" = false
    "enableCRDs" = true
    "galley.enabled" = true
    "gateways.enabled" = false
    "gateways.istio-ingressgateway.resources.limits.cpu" = "2000m"
    "gateways.istio-ingressgateway.resources.limits.memory" = "1024Mi"
    "gateways.istio-ingressgateway.resources.requests.cpu" = "100m"
    "gateways.istio-ingressgateway.resources.requests.memory" = "128Mi"
    "gateways.istio-ingressgateway.type" = "NodePort"
    "global.monitoring.type" = "cluster-monitoring"
    "global.rancher.clusterId" = rancher2_cluster_sync.cluster.cluster_id
    "istio_cni.enabled" = "false"
    "istiocoredns.enabled" = "false"
    "kiali.enabled" = "true"
    "mixer.enabled" = "true"
    "mixer.policy.enabled" = "true"
    "mixer.policy.resources.limits.cpu" = "4800m"
    "mixer.policy.resources.limits.memory" = "4096Mi"
    "mixer.policy.resources.requests.cpu" = "1000m"
    "mixer.policy.resources.requests.memory" = "1024Mi"
    "mixer.telemetry.resources.limits.cpu" = "4800m",
    "mixer.telemetry.resources.limits.memory" = "4096Mi"
    "mixer.telemetry.resources.requests.cpu" = "1000m"
    "mixer.telemetry.resources.requests.memory" = "1024Mi"
    "mtls.enabled" = true
    "nodeagent.enabled" = false
    "pilot.enabled" = true
    "pilot.resources.limits.cpu" = "1000m"
    "pilot.resources.limits.memory" = "4096Mi"
    "pilot.resources.requests.cpu" = "500m"
    "pilot.resources.requests.memory" = "2048Mi"
    "pilot.traceSampling" = "1"
    "security.enabled" = true
    "sidecarInjectorWebhook.enabled" = true
    "tracing.enabled" = true
    "tracing.jaeger.resources.limits.cpu" = "500m"
    "tracing.jaeger.resources.limits.memory" = "1024Mi"
    "tracing.jaeger.resources.requests.cpu" = "100m"
    "tracing.jaeger.resources.requests.memory" = "100Mi"
  }
}