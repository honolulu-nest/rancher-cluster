# Rancher
rancher_count_master = 3
rancher_count_worker = 5
rancher_hostname_master = "islands-rke-m"
rancher_hostname_worker = "islands-rke-w"
rancher_api_url = "rancher.hawaiian.islands.io" #rancher.hawaiian.islands.io
cluster_name = "islands-rke"


# vShpere
vsphere_user = "gitlab@hawaiian.islands.io"
vsphere_vcenter = "vcenter.hawaiian.islands.io"
vsphere_datacenter = "nest"
vsphere_datastore = "vsanDatastore"
vsphere_vm_compute_cluster = "islands"
vsphere_resource_pool = "Resources"
vsphere_network = "trusted"
vsphere_virtual_machine_template = "ubuntu-20.04"
vsphere_folder = "kubernetes"

# NSX
nsx_host = "nsx.hawaiian.islands.io"
nsx_user = "gitlab@hawaiian.islands.io"
kube_api_ip = "10.0.170.4"
nsxt_edge_cluster = "islands-edge"
nsxt_t0 = "islands-t0"
